import React from 'react';
import { Link } from 'react-router-dom';



class Header extends React.Component {
  render() {
    return (
      <div className="ui menu">
        <div className="right aligned menu">
          <Link to="/" className="item">
            <i aria-hidden="true" className="large home icon" />
            Dashboard
          </Link>
          <Link to="/pages/shop" className="item">
            <i aria-hidden="true" className="large fighter jet icon" />
            Shop
          </Link>
          <Link to="/pages/cart" className="item">
            <i className="large shopping cart icon" />
            Cart
          </Link>
        </div>
      </div>
    )
  }
}


export default Header;