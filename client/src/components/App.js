import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Header from './Header';
import Dashboard from './pages/Dashboard';
import Shop from './pages/Shop';
import Cart from './pages/Cart';



class App extends React.Component {
  render(){
    return (
      <div className="ui container">
        <BrowserRouter>
          <Header />
          <Route path="/" exact component={Dashboard} />
          <Route path="/pages/shop" exact component={Shop} />
          <Route path="/pages/cart" exact component={Cart} />
        </BrowserRouter>
      </div>
    )
  }
}

export default App;