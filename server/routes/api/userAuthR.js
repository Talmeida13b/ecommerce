const express = require('express');
const auth = require('../../middleware/auth');
const router = express.Router();
const { check } = require('express-validator');
const userAuthC = require('../../controllers/userAuthC');


//gets user id
router.get('/', auth, userAuthC.getUserById);


//forces user to input required fields for validation
router.post('/', [
    check('email', 'Please include a valid email').isEmail(),
    check('password', 'Password is required').exists()
], userAuthC.login);

module.exports = router;