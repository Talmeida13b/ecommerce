const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const userRegC = require ('../../controllers/userRegC');


//validates if required fields were put in
router.post('/', [
    check('name', 'Name is required')
        .not()
        .isEmpty(),
    check(
        'email',
        'Please include a valid email'
    ).isEmail(),
    check(
        'password',
        'Please enter a password with 6 or more characters'
    ).isLength({ min: 8 })
], userRegC.registerUser);


module.exports = router;