const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    phone_num: {
        type: Number,
    },
    gender: {
        type: String,
    },
    date_of_birth: {
        type: Date,
    },
    avatar: {
        type: String
    },
    tags: {
        type: [],
        default: []
    },
});
const Users = mongoose.model('users', UserSchema);
module.exports = Users;