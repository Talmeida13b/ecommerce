//import "@babel/polyfill";
const express = require('express');
const connectDB = require('./config/db');

const app = express();

// *** Middleware ***
app.use(express.json({ extended: true }));


// *** MongoDB connection ***
connectDB();


app.get('/', (req, res) => res.send('API Running'));


//Routes 
app.use('/api/userRegR', require('./routes/api/userRegR'));
app.use('/api/userAuthR', require('./routes/api/userAuthR'));



//if no enviroment is set, it will connect to port 4000(local)
const PORT = process.env.PORT || 4000;


//where server is running
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));