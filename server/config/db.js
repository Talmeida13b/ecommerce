//database configurations file
const mongoose = require('mongoose');
const config = require('config');
//gets mongoURI key
const db = config.get('mongoURI');


const connectDB = async() => {
  try {
    //getting rid of warnings
    await mongoose.connect(db, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    });

    console.log('MongoDB connected.....')
  } 
  catch(err) {
    console.error(err.message);
    //exit process with failure
    process.exit(1);
  }
}

//export files
module.exports = connectDB;