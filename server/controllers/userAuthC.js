const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const {validationResult} = require('express-validator');
const User = require('../models/userModel');


// @route   GET api/auth (finds user by token)
// @des     Get user by token
// @access  Private
async function getUserById(req, res, next) {
    try {
        const user = await User.findById(req.user.id).select('-password');
        res.json(user);
    }
    catch(err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}



// @route   Post api/auth (signs user in)
// @des     Authenticate user & get token
// @access  Public
async function login(req, res, next) {
//checks for errors during the request phase(if it is a bad request)

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()});
    }
    //destructuring name email and password
    //so that we dont to keep calling req.body
    const {email, password} = req.body;

    try {
        //See if user exists
        let user = await User.findOne({email});

        //if it is not an user, send back an error
        if (!user) {
            return res
                .status(400)
                .json({errors: [{msg: 'Invalid credentials'}]});
        }

        //if user is in database compare passwords to verify credentials
        const isMatch = await bcrypt.compare(password, user.password);

        //if not correct password return error
        if (!isMatch) {
            return res
                .status(400)
                .json({errors: [{msg: 'Invalid Credentials'}]})
        }

        //get payload includes user ID(payload extracts User ID from the database)
        const payload = {
            user: {
                id: user.id
            }
        }
        //signing the token(means to match hashed tokens. checks by seeing if it was created the same way)
        jwt.sign(
            payload,
            config.get('jwtSecret'),
            {expiresIn: 3600000},
            (err, token) => {
                if (err) throw err;
                res.json({token});
            }
        );
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }

}

module.exports = {
    login,
    getUserById,
};