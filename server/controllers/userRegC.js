const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
//express check & validationResult import that validates and checks for errors on registration form
const { validationResult } = require('express-validator');
const User = require('../models/userModel');


async function registerUser(req, res) {
    //bad request error function
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({errors: errors.array()});
    }
    //destructuring name, email and password
    //so that we dont to keep calling req.body
    const {name, email, password} = req.body;

    try {
        //See if user exists by email
        let user = await User.findOne({email});

        //if user already exists return this error message
        if (user) {
            return res.status(400).json({errors: [{msg: 'User already exits'}]});
        }

        //Instance of User. Creates a new user(object)
        user = new User({
            name,
            email,
            avatar,
            password
        });

        //Password encryption(order of operations matters for encryption to work)
        //Creates a salting variable of 10
        const salt = await bcrypt.genSalt(10);

        //Hash(.hash is a javascript function) and salt password
        user.password = await bcrypt.hash(password, salt);

        //Saves new User created to database
        await user.save();

        //get payload includes user ID(payload extracts User ID from the database)
        const payload = {
            user: {
                id: user.id
            }
        }

        //signs the token, passes the payload, token expiration
        //callback function either sends an error back or the token
        //if no error, the token is passed back to the client(User)
        //config.get(jwtSecret) is holding secret key created by us on default json
        jwt.sign(
            payload,
            config.get('jwtSecret'),
            {expiresIn: 3600000},
            (err, token) => {
                if (err) throw err;
                res.json({token});
            }
        );
    }
        //server error if it occurs
    catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }

}

module.exports = {
    registerUser
};